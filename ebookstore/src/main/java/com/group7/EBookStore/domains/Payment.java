package com.group7.EBookStore.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.group7.EBookStore.security.User;

import lombok.Data;

@Data
@Entity
@Table(name="payment")
public class Payment
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="card_number")
	@NotNull
	private String cardNumber;
	
	@Column(name="expiretion_month")
	@NotNull
	private int expiryMonth;
	
	@Column(name="empiretion_year")
	@NotNull
	private int expiryYear;
	
	@Column(name="code")
	@NotNull
	private String code;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="payment_user_id")
	private User username;
	
}
