package com.group7.EBookStore.repostories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.group7.EBookStore.domains.Book;


@Repository
public interface BookRepository extends CrudRepository<Book,Long>
{
	Optional<Book> findBookByTitle(String name);
}
