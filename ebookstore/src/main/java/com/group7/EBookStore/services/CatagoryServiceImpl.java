package com.group7.EBookStore.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import com.group7.EBookStore.domains.Catagory;
import com.group7.EBookStore.repostories.CatagoryRepository;

@Service
public class CatagoryServiceImpl implements CatagoryService 
{
	@Autowired
	public DataSource ds;
	
	@Autowired
	private CatagoryRepository cr;
	
	public JdbcTemplate jt;
	long generatedKey;
	
	public void setDataSource(DataSource ds)
	{
		this.ds=ds;
		this.jt=new JdbcTemplate(ds);
		
	}

	@Override
	public void addCatagory(Catagory catagory) 
	{
		cr.save(catagory);
	}

	@Override
	public void deleteCatagory(long id)
	{
		cr.deleteById(id);
	}

	@Override
	public Iterable<Catagory> findAllCatagorys() 
	{		
		return cr.findAll();
	}

	@Override
	public void updateCatagory(Catagory catagory, long id)
	{
		cr.delete(catagory);
		cr.save(catagory);
		
	}

	@Override
	public Optional<Catagory> findCatagoryById(long id)
	{
		return cr.findById(id);
	}

	@Override
	public Catagory findCatagoryByName(String name) 
	{
		return cr.findCatagoryByName(name);
	}

	

}
