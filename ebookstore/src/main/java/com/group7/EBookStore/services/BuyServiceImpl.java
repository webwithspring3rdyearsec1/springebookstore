package com.group7.EBookStore.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import com.group7.EBookStore.domains.Order;
import com.group7.EBookStore.repostories.BuyRepository;
import com.group7.EBookStore.security.User;

@Service
public class BuyServiceImpl implements BuyService
{
	@Autowired
	public DataSource ds;
	
	@Autowired
	private BuyRepository buyr;
	
	public JdbcTemplate jt;
	long generatedKey;
	
	public void setDataSource(DataSource ds)
	{
		this.ds=ds;
		this.jt=new JdbcTemplate(ds);
		
	}
	@Override
	public void buyBook(String title,int pageNumber,String catagory,String author,int price,String username)
	{
		JdbcTemplate jt=new JdbcTemplate(ds);
		jt.update("insert into mybook (title,page_number,catagory,author,price,username)VALUES(?,?,?,?,?,?)",title,pageNumber,catagory,author,price,username);
		
	}
	@Override
	public List findSoldBook() {
		String sql="select * from buy";
		List<String> books=new ArrayList<String>();
		JdbcTemplate jt=new JdbcTemplate(ds);
		jt.query(sql,new ResultSetExtractor<List>() {
			@Override
			public List extractData(ResultSet rs) throws SQLException, DataAccessException {
				while(rs.next())
				{
					String title=rs.getString("title");
					String author=rs.getString("author");
					String catagory=rs.getString("catagory");
					books.add(title);
					books.add(author);
					books.add(catagory);
				}
				return books;
			}
		});
		return books;
	}
	@Override
	public Iterable<Order> findMyBook(String user) {
		
		return buyr.findAll();
	}
	
}
