package com.group7.EBookStore.repostories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.group7.EBookStore.domains.Order;
import com.group7.EBookStore.security.User;

@Repository
public interface BuyRepository extends CrudRepository<Order,Long> {
	Order findBookByUsername(User user);
}
