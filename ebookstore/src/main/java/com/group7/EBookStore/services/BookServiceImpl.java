package com.group7.EBookStore.services;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.group7.EBookStore.domains.Book;
import com.group7.EBookStore.domains.Catagory;
import com.group7.EBookStore.repostories.BookRepository;
import com.group7.EBookStore.repostories.CatagoryRepository;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	public DataSource ds;
	
	@Autowired
	private BookRepository br;
	
	@Autowired
	private CatagoryRepository cr;
	
	public JdbcTemplate jt;
	long generatedKey;
	
	public void setDataSource(DataSource ds)
	{
		this.ds=ds;
		this.jt=new JdbcTemplate(ds);
		
	}
	
	@Override
	public void addBook(Book book) 
	{
		/*BookForm bf=new BookForm();
		if(bf.getFile() !=null)
		{
			byte[] image=null;
			try
			{
				image=bf.getFile().getBytes();
			}
			catch(IOException e)
			{
				
			}
			if(image != null && image.length > 0)
			{
				book.setImage(image);
			}
			
		}*/
		Catagory c=book.getCatagory();
		book.setCatagory(c);
		br.save(book);
	}

	@Override
	public void deleteBook(long id)
	{
		br.deleteById(id);	
	}


	@Override
	public Iterable<Book> findAllBook()
	{
		return br.findAll();
	}

	@Override
	public Optional<Book> findBookByTitle(String title) 
	{
		return br.findBookByTitle(title);
	}

	@Override
	public Optional<Book> findBookById(long id)
	{
		return br.findById(id);
	}

	@Override
	public void updateBook(Book book)
	{
		jt=new JdbcTemplate();
		
		
	}

	

	
}
