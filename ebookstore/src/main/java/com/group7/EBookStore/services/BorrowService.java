package com.group7.EBookStore.services;

import java.util.List;

import com.group7.EBookStore.domains.BorrowedBooks;

public interface BorrowService
{
	void borrowBook(String title,int pageNumber,String catagory,String author,int price,String usename,String address,String email);
	void returnBook(int id,String title,int pageNumber,String catagory,String author,int price);
	Iterable<BorrowedBooks> findBorrowBook();

}
