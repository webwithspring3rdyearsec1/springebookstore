package com.group7.EBookStore.repostories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.group7.EBookStore.domains.Catagory;

@Repository
public interface CatagoryRepository extends CrudRepository<Catagory,Long> {
	Catagory findCatagoryByName(String name);
}
