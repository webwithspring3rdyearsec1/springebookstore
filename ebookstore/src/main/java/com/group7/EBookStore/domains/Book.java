package com.group7.EBookStore.domains;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
@Entity
@Table(name="book")
public class Book
{	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="title")
	@NotNull()
	private String title;
	
	@Column(name="page_number")
	@NotNull
	@Min(1)
	private int pageNumber;

	@Column(name="author")
	@NotNull()
	private String author;
	
	@Column(name="price")
	@NotNull
	@Min(1)
	private int price;
	
	@Column(name="image")
	private byte[] image;
	
	@ManyToOne
	@JoinTable(name="cata_id")
	public Catagory catagory;
	
	
	@OneToOne(mappedBy = "book", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	private BorrowedBooks borrowedBooks;
	
	@OneToOne(mappedBy = "book", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	private Order order;
	
}
