package com.group7.EBookStore.services;

import java.util.List;

import com.group7.EBookStore.domains.Order;
import com.group7.EBookStore.security.User;

public interface BuyService 
{
	void buyBook(String title,int pageNumber,String catagory,String author,int price,String username);
	List findSoldBook();
	Iterable<Order> findMyBook(String user);
}
