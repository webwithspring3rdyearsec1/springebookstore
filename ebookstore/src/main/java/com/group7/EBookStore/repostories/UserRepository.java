package com.group7.EBookStore.repostories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.group7.EBookStore.security.User;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long>
{ 
	User findByEmail(String email);
}