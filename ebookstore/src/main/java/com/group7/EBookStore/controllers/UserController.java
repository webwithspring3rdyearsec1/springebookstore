package com.group7.EBookStore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.group7.EBookStore.repostories.BorrowRepository;
import com.group7.EBookStore.repostories.BuyRepository;
import com.group7.EBookStore.repostories.CatagoryRepository;
import com.group7.EBookStore.security.User;
import com.group7.EBookStore.services.BookService;
import com.group7.EBookStore.services.BorrowService;
import com.group7.EBookStore.services.BuyService;
import com.group7.EBookStore.services.CatagoryService;


@Controller
public class UserController
{
	@Autowired
	private BookService bs;
	
	@Autowired
	private CatagoryService cs;
	
	@Autowired
	private BorrowService bos;
	
	@Autowired
	private BorrowRepository bor;
	
	@Autowired
	private BuyService bus;
	
	
	@GetMapping("/user/borrowedBooks")
	public String showBorrowedBookPage(Model m,User user)
	{
		m.addAttribute("borrowed",bor.findBookByUsername(user.getUsername()));
		return "/user/borrowedBooks";
	}
	
	@GetMapping("/user/publishBook")
	public String addBook()
	{
		return "/user/publishBook";
	}
	
	@GetMapping("/user/myBook")
	public String showMybookPage(Model m,User user)
	{
		m.addAttribute("borrowed",bus.findMyBook(user.getUsername()));
		return "/user/myBook";
	}
	

	@PostMapping("/buy")
	public String buy(@RequestParam("title") String n,@RequestParam("pageNo")int i,@RequestParam("catagory") String c,@RequestParam("author") String a,@RequestParam("price") int p,@RequestParam("username") String un,Model m)
	{
		bus.buyBook(n,i,c,a,p,un);
		return "/user/myBook";
	}
	
	@PostMapping("/borrow")
	public String borrow(@RequestParam("title") String n,@RequestParam("pageNo")int i,@RequestParam("catagory") String c,@RequestParam("author") String a,@RequestParam("price") int p,@RequestParam("username") String un,@RequestParam("address") String add,@RequestParam("email") String e,Model m)
	{
		bos.borrowBook(n,i,c,a,p,un,add,e);
		return "/user/borrowedBooks";
	}
	
	@GetMapping("/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response)
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
		return "redirect:/login?logout";
	}
	
}
