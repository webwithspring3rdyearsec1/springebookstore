package com.group7.EBookStore.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import com.group7.EBookStore.domains.BorrowedBooks;
import com.group7.EBookStore.repostories.BorrowRepository;

@Service
public class BorrowServiceImpl implements BorrowService
{

	@Autowired
	public DataSource ds;
	
	@Autowired
	private BorrowRepository bor;
	
	public JdbcTemplate jt;
	long generatedKey;
	
	public void setDataSource(DataSource ds)
	{
		this.ds=ds;
		this.jt=new JdbcTemplate(ds);
		
	}
	
	@Override
	public void borrowBook(String title,int pageNumber,String catagory,String author,int price,String username,String address,String email)
	{
		JdbcTemplate jt=new JdbcTemplate(ds);
		jt.update("INSERT INTO borrow(title,page_number,catagory,author,price,username,address,email)VALUES(?,?,?,?,?,?,?,?)",title,pageNumber,catagory,author,price,username,address,email);
		
	}

	@Override
	public void returnBook(int id,String title,int pageNumber,String catagory,String author,int price)
	{
		JdbcTemplate jt=new JdbcTemplate(ds);
		jt.update("INSERT INTO book(title,page_number,catagory,author,price)VALUES(?,?,?,?,?)",title,pageNumber,catagory,author,price);
	    jt.update("DELETE FROM book WHERE id="+id);
			
	}

	@Override
	public Iterable<BorrowedBooks> findBorrowBook()
	{
		return bor.findAll();
	}

	
	

}
