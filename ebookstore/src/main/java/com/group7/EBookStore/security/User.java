package com.group7.EBookStore.security;

import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.group7.EBookStore.domains.BorrowedBooks;
import com.group7.EBookStore.domains.Order;
import com.group7.EBookStore.domains.Payment;

import lombok.Data;

@Data
@Entity
@Table(name = "user")
public class User implements UserDetails {
 
	 @Id
	 @GeneratedValue(strategy = GenerationType.AUTO)
	 private int id;
	 
	 @NotNull()
	 @Column(name = "email")
	 private String email;
	 
	 @NotNull()
	 @Column(name = "firstName")
	 private String firstName; 
	 
	 @NotNull()
	 @Column(name = "lastName")
	 private String lastName;
	 
	 @NotNull()
	 @Column(name = "username")
	 private String username;
	 
	 @NotNull()
	 @Column(name = "password")
	 private String password;
	 
	 @NotNull()
	 @Column(name = "address")
	 private String address;
	 
	
	 @Column(name = "balance")
	 private int balance;
	 
	 @ManyToMany(cascade=CascadeType.ALL)
	 @JoinTable(name="user_role", joinColumns=@JoinColumn(name="user_id"), inverseJoinColumns=@JoinColumn(name="role_id"))
	 private Set<Role> roles;
	 
	 
	 @OneToOne(mappedBy = "username", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	 private BorrowedBooks borrowedBooks;
	 
	 @OneToOne(mappedBy = "username", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	 private Order order;
	 
	 @OneToOne(mappedBy = "username", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	 private Payment payment;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}
}
