package com.group7.EBookStore;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Collections;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.group7.EBookStore.controllers.AdminController;
import com.group7.EBookStore.controllers.LoginController;
import com.group7.EBookStore.repostories.UserRepository;
import com.group7.EBookStore.services.BookService;
import com.group7.EBookStore.services.CatagoryService;
import com.group7.EBookStore.services.UserService;



@RunWith(SpringRunner.class)
@WebMvcTest(controllers = LoginController.class)
public class LoginTest 
{
private MockMvc mockMvc;
	
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @MockBean
    private BookService BookServiceMock;
    
    @MockBean
    private UserRepository ur;
    
    @MockBean
    private CatagoryService CatagoryServiceMock;
    
    @Before(value = "")
    public void setUp() 
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    
    @Test
    public void showLogin() throws Exception {
        assertThat(this.BookServiceMock).isNotNull();
        mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("login"))
                .andExpect(MockMvcResultMatchers.view().name("/login"))
                .andDo(print());
    }
    
    @Test
    public void showRegister() throws Exception {
        assertThat(this.BookServiceMock).isNotNull();
        mockMvc.perform(MockMvcRequestBuilders.get("/register"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("register"))
                .andExpect(MockMvcResultMatchers.view().name("/register"))
                .andDo(print());
    }
    
    @Test
	public void contextLoads() throws Exception {
    	 when(ur.findAll()).thenReturn(
    			 Collections.emptyList());
    	 MvcResult mvcResult=mockMvc.perform(MockMvcRequestBuilders.get("/register").accept(MediaType.APPLICATION_JSON)).andReturn();
    	 System.out.print(mvcResult.getResponse());
    	 verify(ur).findAll();
    	 
	}
}
