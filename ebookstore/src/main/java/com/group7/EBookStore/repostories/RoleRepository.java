package com.group7.EBookStore.repostories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.group7.EBookStore.security.Role;

@Repository("roleRepository")
public interface RoleRepository extends JpaRepository<Role, Integer> 
{
	Role findByType(String type);
}