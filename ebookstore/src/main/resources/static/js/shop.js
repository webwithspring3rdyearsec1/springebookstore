$(function () {
    switch (menu) {
        case 'About':
            $('#about').addClass('active');
            break;
        case 'Contact us':
            $('#contact').addClass('active');
            break;
        case 'Book Lists':
            $('#books').addClass('active');
            break;
        default:
            $('#home').addClass('active')
    }
});