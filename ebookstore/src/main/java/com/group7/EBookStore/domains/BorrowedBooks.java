package com.group7.EBookStore.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.group7.EBookStore.security.User;

import lombok.Data;

@Data
@Entity
@Table(name="borrow")
public class BorrowedBooks 
{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "borrow_book_id")
	private Book book;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "borrow_user_id")
	private User username;
	
	@Column(name="page_number")
	@Size(min=1)
	private int pageNumber;

	@Column(name="author")
	@NotNull()
	private String author;
	
	@Column(name="price")
	@Size(min=1)
	private int price;
	
	@Column(name="email")
	@Email()
	@NotNull()
	private String email;
	
	@Column(name="address")
	@NotNull()
	private String address;

}
