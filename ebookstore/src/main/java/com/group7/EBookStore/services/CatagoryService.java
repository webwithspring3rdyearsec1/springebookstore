package com.group7.EBookStore.services;

import java.util.List;
import java.util.Optional;

import com.group7.EBookStore.domains.Book;
import com.group7.EBookStore.domains.Catagory;

public interface CatagoryService
{
	void addCatagory(Catagory catagory);
	void deleteCatagory(long id);
	void updateCatagory(Catagory catagory,long id);
	Optional<Catagory> findCatagoryById(long id);
	Catagory findCatagoryByName(String name);
	Iterable<Catagory> findAllCatagorys();
	
}
