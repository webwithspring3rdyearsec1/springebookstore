package com.group7.EBookStore.services;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.group7.EBookStore.domains.Book;
import com.group7.EBookStore.domains.Catagory;

public interface BookService {

	void addBook(Book book);
	void deleteBook(long id);
	void updateBook(Book book);
	Optional<Book> findBookByTitle(String name);
	Optional<Book> findBookById(long id);
	Iterable<Book> findAllBook();
}
