package com.group7.EBookStore.controllers;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.group7.EBookStore.domains.Book;
import com.group7.EBookStore.domains.Catagory;
import com.group7.EBookStore.repostories.BorrowRepository;
import com.group7.EBookStore.repostories.BuyRepository;
import com.group7.EBookStore.repostories.CatagoryRepository;
import com.group7.EBookStore.services.BookService;
import com.group7.EBookStore.services.BorrowService;
import com.group7.EBookStore.services.BuyService;
import com.group7.EBookStore.services.CatagoryService;

@Controller
public class AdminController
{
	@Autowired
	private BookService bs;
	
	@Autowired
	private CatagoryService cs;
	
	@Autowired
	private BorrowService bos;
	
	@Autowired
	private BuyService bus;
	

	@GetMapping("/admin/addBook")
	public String addBookPage(Model m,Book book)
	{
		m.addAttribute("book",book);
		m.addAttribute("cata",cs.findAllCatagorys());
		return "/admin/addBook";
	}
	
	@PostMapping("/addBook")
	public String addBook(@Valid Book book,Errors errors,Model m)
	{
		if(errors.hasErrors())
		{
			return "/admin/addBook";
		}
		
		bs.addBook(book);
		return "redirect:/admin/home";
	}
	
	@GetMapping("/delete/{id}")
	public String deletBook(@PathVariable long id)
	{
		bs.deleteBook(id);
		return "redirect:/admin/home";
	}
	
	@GetMapping("/edit/{id}")
	public String editBookPage(@PathVariable long id,Model m,Book book)
	{
		m.addAttribute("books",bs.findBookById(id));
		m.addAttribute("cata",cs.findAllCatagorys());
		return "/admin/edit";
	}
	
	@PutMapping("/edit")
	public String editBookAction(@Valid Book book,Errors errors,Model m)
	{
		if(errors.hasErrors())
		{
			return "/admin/addBook";
		}
		
		return "redirect:/admin/home";
	}
	
	@GetMapping("/admin/catagory")
	public String showCatagoryPage(Model m)
	{
		m.addAttribute("catagory",cs.findAllCatagorys());
		return "/admin/catagory";
	}
	
	@GetMapping("/admin/addCatagory")
	public String showAddCatagoryPage(Model m,Catagory catagory)
	{
		m.addAttribute("catagory",catagory);
		return "/admin/addCatagory";
	}
	
	@PostMapping("/addCatagory")
	public String home(@Valid Catagory catagory,Errors errors,Model m)
	{
		if(errors.hasErrors())
		{
			return "/admin/catagory";
		}
		cs.addCatagory(catagory);
		return "redirect:/admin/catagory";
	}
	
	@DeleteMapping("/deleteCatagory/{id}")
	public String deletCatagory(@PathVariable long id)
	{
		cs.deleteCatagory(id);
		return "redirect:/admin/catagory";
	}
	
	@GetMapping("/editCatagory/{id}")
	public String editCatagoryPage(@PathVariable long id,Model m,Catagory catagory)
	{
		m.addAttribute("edit",cs.findCatagoryById(id));
		return "/admin/editCatagory";
	}
	
	@PostMapping("/editCatagory/{id}")
	public String editCatagory(@Valid Catagory catagory,Errors errors,@PathVariable long id,Model m)
	{
		if(errors.hasErrors())
		{
			return "/admin/editCatagory";
		}
		cs.updateCatagory(catagory, id);;
		return "redirect:/admin/catagory";
	}
	
	@GetMapping("/admin/userRequest")
	public String userRequest(Model m)
	{
		return "/admin/userRequest";
	}
	
	@GetMapping("/admin/borrowedBooks")
	public String showBorrowedBookPage(Model m)
	{
		m.addAttribute("borrowed",bos.findBorrowBook());
		return "/admin/borrowedBooks";
	}
	
	@GetMapping("/admin/soldBook")
	public String showSoldBookPage(Model m)
	{
		m.addAttribute("sold",bus.findSoldBook());
		return "/admin/soldBook";
	}
	
	@GetMapping("/logoutAdmin")
	public String logoutAdmin(HttpServletRequest request, HttpServletResponse response)
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
		return "redirect:/login?logout";
	}

	
}
