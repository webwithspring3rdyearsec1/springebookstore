package com.group7.EBookStore.services;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.group7.EBookStore.security.User;

public interface UserService extends UserDetailsService {
	
	 public User findUserByEmail(String email);
	 
	 public void saveUser(User user);

}
