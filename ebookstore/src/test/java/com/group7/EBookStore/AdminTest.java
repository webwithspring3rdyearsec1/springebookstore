package com.group7.EBookStore;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.group7.EBookStore.controllers.AdminController;
import com.group7.EBookStore.services.BookService;
import com.group7.EBookStore.services.CatagoryService;



@RunWith(SpringRunner.class)
@WebMvcTest(controllers = AdminController.class)
public class AdminTest 
{
	private MockMvc mockMvc;
	
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @MockBean
    private BookService BookServiceMock;
    
    @MockBean
    private CatagoryService CatagoryServiceMock;
    
    @Before(value = "")
    public void setUp() 
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    
    @Test
    public void showCatagory() throws Exception {
        assertThat(this.BookServiceMock).isNotNull();
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/catagory"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("catagory"))
                .andExpect(MockMvcResultMatchers.view().name("/admin/catagory"))
                .andDo(print());
    }
    
    @Test
    public void showAddBook() throws Exception {
        assertThat(this.BookServiceMock).isNotNull();
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/addBook"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("addBook"))
                .andExpect(MockMvcResultMatchers.view().name("/admin/addBook"))
                .andDo(print());
    }
	
    @Test
    public void showSoldBookPage() throws Exception {
        assertThat(this.BookServiceMock).isNotNull();
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/soldBook"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("soldBook"))
                .andExpect(MockMvcResultMatchers.view().name("/admin/soldBook"))
                .andDo(print());
    }
    
    @Test
    public void showBorrowedBookPage() throws Exception {
        assertThat(this.BookServiceMock).isNotNull();
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/borrowedBooks"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("borrowedBooks"))
                .andExpect(MockMvcResultMatchers.view().name("/admin/borrowedBooks"))
                .andDo(print());
    }
    
    
    @Test
    public void userRequest() throws Exception {
        assertThat(this.BookServiceMock).isNotNull();
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/userRequest"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("userRequest"))
                .andExpect(MockMvcResultMatchers.view().name("/admin/userRequest"))
                .andDo(print());
    }
    
    @Test
    public void addBookPost() throws Exception
    {
    	((ResultActions) ((MockHttpServletRequestBuilder) mockMvc.perform(post("/admin/addBook")))
    		   .content("title=Introduction to Spring & author=David Moss & catagory_id=2 & page_number=450 & price=150")
    		   .contentType(MediaType.APPLICATION_FORM_URLENCODED))
			   .andExpect(status().is3xxRedirection())
			   .andExpect(header().stringValues("Location", "/admin/home"));
    }
    
    @Test
    public void addCatagoryPost() throws Exception
    {
    	((ResultActions) ((MockHttpServletRequestBuilder) mockMvc.perform(post("/admin/addCatagory")))
    		   .content("name=astrology")
    		   .contentType(MediaType.APPLICATION_FORM_URLENCODED))
			   .andExpect(status().is3xxRedirection())
			   .andExpect(header().stringValues("Location", "/admin/catagory"));
    }
    
    
}
