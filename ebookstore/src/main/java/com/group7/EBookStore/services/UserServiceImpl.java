package com.group7.EBookStore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.group7.EBookStore.security.Role;
import com.group7.EBookStore.security.User;
import com.group7.EBookStore.repostories.RoleRepository;
import com.group7.EBookStore.repostories.UserRepository;
import java.util.Arrays;
import java.util.HashSet;

@Service("userService")
public class UserServiceImpl implements UserService {
 
 @Autowired
 private UserRepository userRepository;
 
 @Autowired
 private RoleRepository roleRespository;
 
 @Autowired
 private BCryptPasswordEncoder bCryptPasswordEncoder;

 @Override
 public User findUserByEmail(String email) {
	 return userRepository.findByEmail(email);
 }

 @Override
 public void saveUser(User user) {
	 user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
	 user.setBalance(1000);
	 Role userRole = roleRespository.findByType("ADMIN");
	 user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
	 userRepository.save(user);
 }

@Override
public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
	User user=userRepository.findByEmail(email);
	if(user != null) {
		return user;
	}
	throw new UsernameNotFoundException("User by email  '" + email + "' not found");
}

}