package com.group7.EBookStore.domains;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.group7.EBookStore.security.User;

import lombok.Data;

@Data
@Entity
@Table(name="buy")
public class Order 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="title")
	@NotNull()
	private String title;
	
	@Column(name="page_number")
	@Size(min=1)
	private int pageNumber;
	
	@Column(name="catagory")
	@NotNull()
	private String catagory;
	
	@Column(name="author")
	@NotNull()
	private String author;
	
	@Column(name="price")
	@Size(min=1)
	private int price;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "buy_book_id")
	private Book book;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "buy_user_id")
	private User username;
	
}
