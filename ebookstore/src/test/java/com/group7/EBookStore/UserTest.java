package com.group7.EBookStore;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.group7.EBookStore.controllers.AdminController;
import com.group7.EBookStore.controllers.UserController;
import com.group7.EBookStore.services.BookService;
import com.group7.EBookStore.services.CatagoryService;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UserController.class)
public class UserTest
{
private MockMvc mockMvc;
	
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @MockBean
    private BookService BookServiceMock;
    
    @MockBean
    private CatagoryService CatagoryServiceMock;
    
    @Before(value = "")
    public void setUp() 
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    
    @Test
    public void showBorrowedBooks() throws Exception {
        assertThat(this.BookServiceMock).isNotNull();
        mockMvc.perform(MockMvcRequestBuilders.get("/user/borrowedBooks"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("borrowedBooks"))
                .andExpect(MockMvcResultMatchers.view().name("/user/borrowedBooks"))
                .andDo(print());
    }
    
    @Test
    public void showMyBooks() throws Exception {
        assertThat(this.BookServiceMock).isNotNull();
        mockMvc.perform(MockMvcRequestBuilders.get("/user/myBook"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("myBook"))
                .andExpect(MockMvcResultMatchers.view().name("/user/myBook"))
                .andDo(print());
    }
    
    @Test
    public void showPublishBookPage() throws Exception {
        assertThat(this.BookServiceMock).isNotNull();
        mockMvc.perform(MockMvcRequestBuilders.get("/user/publishBook"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("publishBook"))
                .andExpect(MockMvcResultMatchers.view().name("/user/publishBook"))
                .andDo(print());
    }
    
    @Test
    public void borrowBookPost() throws Exception
    {
    	((ResultActions) ((MockHttpServletRequestBuilder) mockMvc.perform(post("/user/borrow")))
    		   .content("user_id=3 & book_id=45 & borrowedDate=2/23/2019 & returnDate=2/30/2019 & price=70")
    		   .contentType(MediaType.APPLICATION_FORM_URLENCODED))
			   .andExpect(status().is3xxRedirection())
			   .andExpect(header().stringValues("Location", "/user/borrowedBooks"));
    }
    
    @Test
    public void buyBookPost() throws Exception
    {
    	((ResultActions) ((MockHttpServletRequestBuilder) mockMvc.perform(post("/user/buy")))
    		   .content("user_id=3 & book_id=45 & price=70")
    		   .contentType(MediaType.APPLICATION_FORM_URLENCODED))
			   .andExpect(status().is3xxRedirection())
			   .andExpect(header().stringValues("Location", "/user/myBooks"));
    }
    
}
