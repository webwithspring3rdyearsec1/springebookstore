package com.group7.EBookStore.repostories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.group7.EBookStore.domains.BorrowedBooks;
import com.group7.EBookStore.security.User;

@Repository
public interface BorrowRepository extends CrudRepository<BorrowedBooks,Long>
{
	BorrowedBooks findBookByUsername(String string);
}
