package com.group7.EBookStore.domains;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.Data;

@Data
public class Notification
{
	
	public String messag;
	
	@Enumerated(EnumType.STRING)
	private final Type type;
	
	public static enum Type
	{
		WARNING,INFORMATION
	}
}
