package com.group7.EBookStore.configuration;

import javax.sql.DataSource;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import com.group7.EBookStore.configuration.SimpleAuthenticationSuccessHandler;;
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter{

	 @Autowired
	 public SimpleAuthenticationSuccessHandler successHandler;
	
	 @Autowired
	 private BCryptPasswordEncoder bCryptPasswordEncoder;
	 
	 @Autowired
	 private DataSource dataSource;
	 
	 private final String USERS_QUERY = "select email, password, balance from user where email=?";
	 private final String ROLES_QUERY = "select u.email, r.type from user u inner join user_role ur on (u.id = ur.user_id) inner join role r on (ur.role_id=r.id) where u.email=?";
	
	 @Override
	 protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		  auth.jdbcAuthentication()
		   .usersByUsernameQuery(USERS_QUERY)
		   .authoritiesByUsernameQuery(ROLES_QUERY)
		   .dataSource(dataSource)
		   .passwordEncoder(bCryptPasswordEncoder);
	 }
	 
	 @Override
	 protected void configure(HttpSecurity http) throws Exception{
	  http.authorizeRequests()
		   .antMatchers("/").permitAll()
		   .antMatchers("/login").permitAll()
		   .antMatchers("/signup").permitAll()
		   .antMatchers("/admin/**").hasAuthority("ADMIN")
		   .antMatchers("/user/**").hasAuthority("USER").anyRequest()
		   .authenticated().and().csrf().disable()
		   .formLogin().loginPage("/login")
		   .successHandler(successHandler)
		   .failureUrl("/login?error=true")
		   .usernameParameter("email")
		   .passwordParameter("password")
		   .and().logout()
		   .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
		   .logoutSuccessUrl("/")
		   .and().rememberMe()
		   .tokenRepository(persistentTokenRepository())
		   .tokenValiditySeconds(60*60)
		   .and().exceptionHandling().accessDeniedPage("/access_denied")
		   .and()
		   .csrf()
		   .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
		 }
	 
	 @Bean
	 public PersistentTokenRepository persistentTokenRepository() {
		  JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
		  db.setDataSource(dataSource);
		  
		  return db;
	 }
	 @Override
	 public void configure(WebSecurity webSecurity) throws Exception {
		 
		 webSecurity.ignoring()
		 			.antMatchers("/resources/**","/static/**","/css/**","/js/**","/images/**","/node_modules/**","/vendor/**","/fonts/**");
		 
	 }
}