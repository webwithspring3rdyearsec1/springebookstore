package com.group7.EBookStore.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.group7.EBookStore.domains.Book;
import com.group7.EBookStore.repostories.BookRepository;
import com.group7.EBookStore.repostories.CatagoryRepository;
import com.group7.EBookStore.security.User;
import com.group7.EBookStore.services.BookService;
import com.group7.EBookStore.services.CatagoryService;
import com.group7.EBookStore.services.UserService;

@Controller
public class LoginController 
{

	 @Autowired
	 private UserService userService;
	 
	 @Autowired
	 private CatagoryService cs;
	 
	 @Autowired 
	 public BookRepository br;
	 
	 @Autowired
	 private BookService bs;
	 
	 public ArrayList<Book> books;
	 public List<String>  book;
	 
	 @RequestMapping(value= {"/login"}, method=RequestMethod.GET)
	 public String login(Model m) {
		 return "login";
	 }
	 
	 @RequestMapping(value= {"/signup"}, method=RequestMethod.GET)
	 public String signup(Model m,User user) {
		 m.addAttribute("user", user);
		 return "signup";
	 }
	 
	 @RequestMapping(value= {"/signup"}, method=RequestMethod.POST)
	 public String createUser(@Valid User user, BindingResult bindingResult,Model m)
	 {
		  User userExists = userService.findUserByEmail(user.getEmail());
		  
		  if(userExists != null) 
		  {
			  bindingResult.rejectValue("email", "error.user", "This email already exists!");
		  }
		  
		  if(bindingResult.hasErrors()) 
		  {
		   
		  } 
		  
		  else
		  {
			   userService.saveUser(user);
			   m.addAttribute("msg", "User has been registered successfully!");
			   m.addAttribute("user", new User());
		  }
		  
		  return "signup";
	 }
	 
	 @RequestMapping(value= {"/admin/home"}, method=RequestMethod.GET)
	 public String adminHome(Authentication auth,User user,Model m)
	 {
		 auth = SecurityContextHolder.getContext().getAuthentication();
		 user = userService.findUserByEmail(auth.getName());	  
		 m.addAttribute("userName", user.getFirstName() + " " + user.getLastName());
		 m.addAttribute("books",bs.findAllBook());
		 return "admin/home";
	 }
	 
	 @RequestMapping(value= {"/user/user"}, method=RequestMethod.GET)
	 public String userHome(Authentication auth,User user,Model m) 
	 {
		 auth = SecurityContextHolder.getContext().getAuthentication();
		 user = userService.findUserByEmail(auth.getName());	  
		 m.addAttribute("userName", user.getFirstName() + " " + user.getLastName());
		 m.addAttribute("catagory",cs.findAllCatagorys());
		 return "user/user";
	 }
	 
	 
	 @RequestMapping(value= {"/access_denied"}, method=RequestMethod.GET)
	 public String accessDenied(Model m)
	 {
		 return "errors/access_denied";
	 }
	}
